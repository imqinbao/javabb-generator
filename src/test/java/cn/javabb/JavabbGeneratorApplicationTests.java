package cn.javabb;

import cn.javabb.generator.Generator;
import cn.javabb.generator.JavabbGeneratorApplication;
import cn.javabb.generator.config.DataSourceConfig;
import cn.javabb.generator.config.GenConfig;
import cn.javabb.generator.config.PackageConfig;
import cn.javabb.generator.engine.GenTemplateEngine;
import cn.javabb.generator.model.GenModel;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JavabbGeneratorApplication.class)
class JavabbGeneratorApplicationTests {

    @Test
    void contextLoads() {

    }
    @Test
    public void test() throws Exception {
        GenModel genModel = new GenModel()
                .setModelName("sys")
                .setPrefix(Arrays.asList("sys_"))
                .setTables(Arrays.asList("sys_user", "sys_role"));
        GenModel genModel2 = new GenModel()
                .setModelName("biz")
                .setPrefix(Arrays.asList("biz_"))
                .setTables(Arrays.asList("biz_blog", "biz_post"));
        List<GenModel> models = Arrays.asList(genModel,genModel2);

        DataSourceConfig dsc = new DataSourceConfig()
                .setUrl("jdbc:mysql://59.110.236.115:3306/db_portal?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8")
                .setUsername("db_test")
                .setPassword("123456")
                .setDriverName("com.mysql.cj.jdbc.Driver");

        GenConfig genConfig = new GenConfig()
                .setDebugModel(false)
                .setTplName("tkmapper-tpl")
                .setAuthor("javabb")
                .setProjectName("EasyWeb")
                .setPackageName("cn.javabb")
                .setGroupId("cn.javabb")
                .setModels(models);

        Generator generator = new Generator.GeneratorBuilder()
                .setDataSourceConfig(dsc)
                .setGenConfig(genConfig)
                .setPackageConfig(new PackageConfig())
                .build();
        generator.gen();
        /*GeneratorHelper generator = new GeneratorHelper(dbConfig);
        List<TableInfo> list = generator.getTableInfos(new CodeConfig());
        list.forEach(t -> System.out.println(t.toString()));*/
    }

    @Test
    public void test1(){
        GenTemplateEngine templateEngine = new GenTemplateEngine("", "tkmapper-tpl");
        GenConfig genConfig = new GenConfig();
        //基本信息配置
        genConfig.setAuthor("javabb");
        genConfig.setPackageName("cn.javabb2");
        genConfig.setTplName("tkmapper-tpl");
        //设置模块
        List<GenModel> models = new ArrayList<>();
        GenModel model = new GenModel();
        model.setTables(Arrays.asList("sys_user","sys_role","sys_link"));
        model.setModelName("sys");
        model.setPrefix(Arrays.asList("sys_"));
        models.add(model);
        genConfig.setModels(models);

        //GenUtil.genCurd("D:/temp/",genConfig,templateEngine);
    }

    @Test
    public void test3() {
        String[] strings = "aaa,bbbb".split(",");
        System.out.println(strings[1]);
    }

}
