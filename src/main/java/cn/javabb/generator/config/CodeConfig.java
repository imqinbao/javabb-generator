package cn.javabb.generator.config;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @desc:   策略配置
 * @author: javabb (javabob(a)163.com)
 * @create: 2020/07/14 23:45
 */
@Data
@Accessors(chain = true)
public class CodeConfig {
    /**
     * 是否为lombok模型,默认不开启
     * <br>lombok文档地址:
     * <a href="https://projectlombok.org/">document</a>
     */
    private boolean entityLombokModel = false;
    /**
     * 是否实体驼峰命名,默认驼峰命名
     */
    private boolean entityCamelModel = true;
    /**
     * 是否生成builder代码,默认不开启
     */
    private boolean entityBuilderModel = false;
    /**
     * 是否生成swagger代码,默认不开启
     */
    private boolean swagger = false;
}
