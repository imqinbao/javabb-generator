package cn.javabb.generator.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @desc:   数据库配置
 * @author: javabb (javabob(a)163.com)
 * @create: 2020/07/11 00:33
 */
@Component
@ConfigurationProperties(prefix = "db")
@PropertySource(value = { "classpath:generator.yml" })
public class DbConfig {
    /** 数据库连接地址 */
    public static String dbUrl;
    /** 数据库用户名 */
    public static String dbUserName;
    /** 数据库密码 */
    public static String dbPassword;
    /** 数据库驱动 */
    public static String dbDriverName;

    public static String getDbUrl() {
        return dbUrl;
    }
    @Value("${dbUrl}")
    public void setDbUrl(String dbUrl) {
        DbConfig.dbUrl = dbUrl;
    }

    public static String getDbUserName() {
        return dbUserName;
    }
    @Value("${dbUserName}")
    public void setDbUserName(String dbUserName) {
        DbConfig.dbUserName = dbUserName;
    }

    public static String getDbPassword() {
        return dbPassword;
    }
    @Value("${dbPassword}")
    public void setDbPassword(String dbPassword) {
        DbConfig.dbPassword = dbPassword;
    }

    public static String getDbDriverName() {
        return dbDriverName;
    }
    @Value("${dbDriverName}")
    public void setDbDriverName(String dbDriverName) {
        DbConfig.dbDriverName = dbDriverName;
    }
}
